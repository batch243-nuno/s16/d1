// console.log("Hello World")

// Arithmetic Operators

let x = 1397, y = 7831;
console.log(`The value of x is: ${x}`);
console.log(`The value of y is: ${y}`);
// Addition
let sum = x + y;
console.log(`Result of Addition Operator: ${sum}`);

// Subtraction
let difference = x - y;
console.log(`Result of Subtraction Operator: ${difference}`);

// Multiplication
let product = x * y;
console.log(`Result of Multiplication Operator: ${product}`);

// Division
let quotient = x / y;
console.log(`Result of Division Operator: ${quotient}`);

// Modulo
let remainder = y % x;
console.log(`Result of Modulo Operator: ${remainder}`);
let secondRemainder = x % y;
console.log(`Result of Modulo Operator: ${secondRemainder}`);

// Assignment Operators

let assignmentNumber = 8;
// Basic Assignment Operator

// Addition Assigment Operator
assignmentNumber += 2;
console.log(`Result of Addition Assignment Operator: ${assignmentNumber}`);


// Subraction Assigment Operator
assignmentNumber -= 2;
console.log(assignmentNumber);
console.log(`Result of Subratction Assignment Operator: ${assignmentNumber}`);

// Multiplication Assigment Operator
assignmentNumber *= 4;
console.log(`Result of Multiplication Assignment Operator: ${assignmentNumber}`);

// Division Assigment Operator
assignmentNumber /= 8;
console.log(`Result of Division Assignment Operator: ${assignmentNumber}`);

// Multiple Operators and Parenthesis
let mdas = 1 + 2 - 3 * 4 / 5;
/*
    1. 3*4 = 12
    2. 12/5 = 2.4
    3. 1+2 = 3
    4. 3-2.4 = 0.6
*/
console.log(`Result of MDAS rule is: ${mdas}`);

let pemdas = 1 + (2 -3) * (4 / 5);
console.log(`Result of PEMDAS rule is: ${pemdas}`);

// Incrementaion and Decremention

let z = 1;
// Pre-Increment
let increment = ++z;

console.log(`Result of z in Pre-Increment: ${z}`);
console.log(`Result of increment in Pre-Increment: ${increment}`);
// Post-Increment
increment = z++;
console.log(`Result of z in Post-Increment: ${z}`);
console.log(`Result of increment in Pre-Increment: ${increment}`);
increment = z++;
console.log(`Result of increment in Pre-Increment: ${increment}`);

let p = 0;
// Pre-Decrement
let decrement = --p;
console.log(`Result of p in Pre-Increment: ${p}`);
console.log(`Result of increment in Pre-Increment: ${decrement}`);

// Post-decrement
decrement = p--;
console.log(`Result of p in Post-decrement: ${p}`);
console.log(`Result of decrement in Pre-decrement: ${decrement}`);
decrement = p--;
console.log(`Result of decrement in Pre-decrement: ${decrement}`);

// Type Coercion
    // automatic conversion of value from one data type to another
let numA = `10`
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

// Non-Coercion
let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = false + 1;
console.log(numE);

// Comparison Operators
let juan = `juan`;

// Equility Operator
let isEqual = 1 == 1;
console.log(typeof isEqual);
isEqual = "1" == 1;
console.log(isEqual);

// Strict Equality Operator
isEqual = "1" === 1;
console.log(isEqual);

// Inequality Operator
let isNotequal = "1" != 2;
console.log(isNotequal);

// Relational Operators

let a = 50;
let b = 65;
// Greater than
let isGreater = a > b;
console.log(isGreater);

// Less Than
let isLess = a < b;
console.log(isLess);

// Greater Than or Equal
let isGTOrEqual = a >= b;
console.log(isGTOrEqual);

// Less Than or Equal
let isLTOrEqual = a <= b;
console.log(isLTOrEqual);

let numStr = `30`;

console.log( a > numStr);

// Logical Operator
let isLegalAge = true;
let isRegistered = false;

// Logical AND
// only true if both statement is true
let allRequirement = isLegalAge && isRegistered;
console.log(`And : ${allRequirement}`);

// Logical OR
allRequirement = isLTOrEqual || isRegistered;
console.log(`Or : ${allRequirement}`);

// Logical Not
allRequirement = !isRegistered
console.log(`Not : ${allRequirement}`);
